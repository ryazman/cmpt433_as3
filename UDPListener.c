/*
 * UDPListener creates a socket and listens for UDP commands (in this case, from
 * the BeatBox web interface. It then makes calls to the BeatBox module to change
 * beats, tempo, and volume.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "UDPListener.h"
#include "BeatBox.h"

#define BUFFER_LENGTH 1024
#define PORT_NUMBER 12345

static _Bool isUDPFinished = false;


_Bool UDPListener_isFinished() {
	return isUDPFinished;
}

static void die(char* errorMessage)
{
	printf("%s\n", errorMessage);
	exit(-1);
}

static void changeBeat(char responseMessageBuffer[], char* beatName)
{
	BeatBox_changeBeat(beatName);
	char* currBeatName = BeatBox_getCurrBeatName();
	sprintf(responseMessageBuffer, "%s", currBeatName);
}

static void getBeat(char responseMessageBuffer[])
{
	char* currBeatName = BeatBox_getCurrBeatName();
	sprintf(responseMessageBuffer, "%s", currBeatName);
}

static void increaseVolume(char responseMessageBuffer[])
{
	BeatBox_increaseVolume();
	int currVolume = BeatBox_getVolume();
	char volStr[3];
	sprintf(volStr, "%d", currVolume);
	sprintf(responseMessageBuffer, "%s", volStr);
}

static void decreaseVolume(char responseMessageBuffer[])
{
	BeatBox_decreaseVolume();
	int currVolume = BeatBox_getVolume();
	char volStr[3];
	sprintf(volStr, "%d", currVolume);
	sprintf(responseMessageBuffer, "%s", volStr);
}

static void getVolume(char responseMessageBuffer[])
{
	int currVolume = BeatBox_getVolume();
	char volStr[3];
	sprintf(volStr, "%d", currVolume);
	sprintf(responseMessageBuffer, "%s", volStr);
}

static void increaseTempo(char responseMessageBuffer[])
{
	BeatBox_increaseTempo();
	int currTempo = BeatBox_getTempo();
	char tempoStr[3];
	sprintf(tempoStr, "%d", currTempo);
	sprintf(responseMessageBuffer, "%s", tempoStr);
}

static void decreaseTempo(char responseMessageBuffer[])
{
	BeatBox_decreaseTempo();
	int currTempo = BeatBox_getTempo();
	char tempoStr[3];
	sprintf(tempoStr, "%d", currTempo);
	sprintf(responseMessageBuffer, "%s", tempoStr);
}

static void getTempo(char responseMessageBuffer[])
{
	int currTempo = BeatBox_getTempo();
	char tempoStr[3];
	sprintf(tempoStr, "%d", currTempo);
	sprintf(responseMessageBuffer, "%s", tempoStr);
}

static void playSound(char responseMessageBuffer[], char* soundName)
{
	BeatBox_playSound(soundName);
	sprintf(responseMessageBuffer, "%s", soundName);
}

static void getUptime(char responseMessageBuffer[])
{
	struct sysinfo info;
	sysinfo(&info);
	//Uptime parsing code from: http://stackoverflow.com/questions/3070278/uptime-under-linux-in-c
	sprintf(responseMessageBuffer, "Device up for:\n%02ld:%02ld:%02ld(H:M:S)", info.uptime/3600, info.uptime%3600/60, info.uptime%60);
}

static void testReply(char responseMessageBuffer[])
{
	sprintf(responseMessageBuffer, "up");
}

static void generateBeatBoxAction(char message[], char responseMessageBuffer[])
{
	//parse message and call appropriate print call
//	printf("Message: %s\n", message);
	char* command = strtok(message, " \n");
//	printf("Command: %s\n", command);
	char* commandArg = strtok(NULL, " ");
	int commandSize = strlen(command);
//	printf("Command size: %d\n", commandSize);

	if(strncmp(command, "changeBeat", commandSize) == 0) {
		changeBeat(responseMessageBuffer, commandArg);
	} else if (strncmp(command, "getBeat", commandSize) == 0) {
		getBeat(responseMessageBuffer);
	} else if (strncmp(command, "increaseVolume", commandSize) == 0) {
		increaseVolume(responseMessageBuffer);
	} else if (strncmp(command, "decreaseVolume", commandSize) == 0) {
		decreaseVolume(responseMessageBuffer);
	} else if (strncmp(command, "getVolume", commandSize) == 0) {
		getVolume(responseMessageBuffer);
	} else if (strncmp(command, "increaseTempo", commandSize) == 0) {
		increaseTempo(responseMessageBuffer);
	} else if (strncmp(command, "decreaseTempo", commandSize) == 0) {
		decreaseTempo(responseMessageBuffer);
	} else if (strncmp(command, "getTempo", commandSize) == 0) {
		getTempo(responseMessageBuffer);
	} else if (strncmp(command, "playSound", commandSize) == 0) {
		playSound(responseMessageBuffer, commandArg);
	} else if (strncmp(command, "getUptime", commandSize) == 0) {
		getUptime(responseMessageBuffer);
	} else if (strncmp(command, "testReply", commandSize) == 0) {
		testReply(responseMessageBuffer);
	} else {
//		printf("Should not get here!\n");
	}
}

static void listenForPackets(int UDPSocket, struct sockaddr_in hostSocketAddress)
{
	int messageLength;
	int responseMessageLength;
	int socketLength = sizeof(hostSocketAddress);
	char messageBuffer[BUFFER_LENGTH];

    while(!isUDPFinished) {
        fflush(stdout);
		char responseMessageBuffer[BUFFER_LENGTH];

        //try to receive some data, this is a blocking call
        messageLength = recvfrom(UDPSocket, messageBuffer, BUFFER_LENGTH, 0, (struct sockaddr *) &hostSocketAddress, (socklen_t*) &socketLength);

        if (messageLength == -1) {
        	die("Message receive failed.\n");
        }

        generateBeatBoxAction(messageBuffer, responseMessageBuffer);

        responseMessageLength = sendto(UDPSocket, responseMessageBuffer, strlen(responseMessageBuffer), 0, (struct sockaddr*) &hostSocketAddress, (socklen_t) socketLength);

        if (responseMessageLength == -1) {
        	die("Message sent failed.\n");
        }

        memset(&responseMessageBuffer[0], 0, sizeof(responseMessageBuffer));
    }
}

void* UDPListener_setupSocket()
{
		struct sockaddr_in targetSocketAddress, hostSocketAddress;

	    int UDPSocket;

	    //create a UDP socket
	    UDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	    if (UDPSocket == -1) {
	    	die("Socket creation failed.\n");
	    }

	    // zero out the structure
	    memset((char *) &targetSocketAddress, 0, sizeof(targetSocketAddress));

	    targetSocketAddress.sin_family = AF_INET;
	    targetSocketAddress.sin_port = htons(PORT_NUMBER);
	    targetSocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	    //bind socket to port
	    int bindReturn = bind(UDPSocket ,(struct sockaddr*)&targetSocketAddress, sizeof(targetSocketAddress));

	    if(bindReturn == -1) {
	    	die("Binding failed.\n");
	    }

	    //keep listening for data
	    listenForPackets(UDPSocket, hostSocketAddress);

	    close(UDPSocket);

	    return NULL;
}

int UDPListener_createUDPListenerThread() {
	pthread_t udpListenerID;
	pthread_create(&udpListenerID, NULL, &UDPListener_setupSocket, NULL);

	return udpListenerID;
}



