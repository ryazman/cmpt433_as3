/*
 * BeatBox.h
 */

#ifndef BEATBOX_H_
#define BEATBOX_H_

void BeatBox_nextBeat();
void BeatBox_changeBeat(char* beatName);
void BeatBox_increaseVolume();
void BeatBox_decreaseVolume();
void BeatBox_increaseTempo();
void BeatBox_decreaseTempo();
void BeatBox_playSound(char* soundName);
int BeatBox_getVolume();
int BeatBox_getTempo();
char* BeatBox_getCurrBeatName();
void BeatBox_queueHiHat();
void BeatBox_queueSnare();
void BeatBox_queueBaseDrum();
_Bool BeatBox_isFinished();

#endif /* BEATBOX_H_ */
