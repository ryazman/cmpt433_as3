OUTFILE = BeatBox
OUTDIR = $(HOME)/cmpt433/public/myApps/

CROSS_COMPILE = arm-linux-gnueabi-
CC_C = $(CROSS_COMPILE)gcc
CFLAGS = -Wall -g -std=c99 -D _POSIX_C_SOURCE=200809L -Werror -pthread -lrt
LFLAGS = -L$(HOME)/cmpt433/public/asound_lib_BBB

SRC = audioMixer.c UDPListener.c JoystickReader.c BeatBox.c accelerometer.c
OBJ = audioMixer.o UDPListener.o JoystickReader.o BeatBox.o accelerometer.o

all: wav $(OUTFILE) makeserver

$(OUTFILE): $(OBJ)
	$(CC_C) $(CFLAGS) -o $(OUTDIR)/$(OUTFILE) $(OBJ) $(LFLAGS) -lpthread -lasound
	
wav:
	mkdir -p $(OUTDIR)beatbox-wav-files/ 
	cp -R beatbox-wave-files/* $(OUTDIR)beatbox-wav-files/ 
	
makeserver:
	cd beatbox-server && $(MAKE)

%.o : %.c
	$(CC_C) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJ)
	rm -f $(OUTDIR)/$(OUTFILE)

