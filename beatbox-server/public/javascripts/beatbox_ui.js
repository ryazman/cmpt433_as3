"use strict";
// Client-side interactions with the browser.

// Make connection to server when web page is fully loaded.
var socket = io.connect();
$(document).ready(function() {
	
	window.setInterval(function() {
		sendBeatCommand("getBeat ");
		sendUptimeCommand("getUptime ");
		sendVolumeCommand("getVolume ");
		sendTempoCommand("getTempo ");
		
	}, 1000);
	
	var serverTimer;
	window.setInterval(function() {
		testServer("hey");
		serverTimer = setTimeout(function() {
			displayServerError();
		}, 1000);
	}, 1000);
/*What you likely need in the client is:
	- a timer (TimeA, say) which every second emits a message to the server, to which the server will reply.
	- a timer (timerB, say) which is started when the above poll is sent. It is only started when it is not already running (i.e., don't start two of them at once). You can "save" the timer by storing the return value from the setInterval() method).
	- when you get a reply from the server, stop TimerB.
	- when TimerB expires, then you know you have a communication problem.*/

	// Beat Mode buttons
	$('#modeNone').click(function(){
		sendBeatCommand("changeBeat 0 ");
	});
	$('#modeRock1').click(function(){
		sendBeatCommand("changeBeat 1 ");
	});
	$('#modeRock2').click(function(){
		sendBeatCommand("changeBeat 2 ");
	});

	// Volume buttons
	$('#volumeDown').click(function(){
		sendVolumeCommand("decreaseVolume ");
	});
	$('#volumeUp').click(function(){
		sendVolumeCommand("increaseVolume ");
	});
	
	// Tempo buttons
	$('#tempoDown').click(function(){
		sendTempoCommand("decreaseTempo ");
	});
	$('#tempoUp').click(function(){
		sendTempoCommand("increaseTempo ");
	});
	
	// Individual sound buttons
	$('#playHiHat').click(function(){
		sendSoundCommand("playSound h ");
	});
	$('#playSnare').click(function(){
		sendSoundCommand("playSound s ");
	});
	$('#playBass').click(function(){
		sendSoundCommand("playSound b ");
	});

	handleBeatReply();
	handleVolumeReply();
	handleTempoReply();
	handleUptimeReply();

	//Wait for server response
	socket.on('testReply', function(result) {
		console.log('testReply: ' + result);
		clearTimeout(serverTimer);
	});
});

function sendBeatCommand(message) {
	socket.emit('beat', message);
};

function sendVolumeCommand(message) {
	socket.emit('volume', message);
};

function sendTempoCommand(message) {
	socket.emit('tempo', message);
};

function sendSoundCommand(message) {
	socket.emit('sound', message);
};

function sendUptimeCommand(message) {
	socket.emit('uptime', message);
};

function testServer(message) {
	socket.emit('test', message);
	return message;
};

// Set current beat display on page
function handleBeatReply() {
	socket.on('beatReply', function(result) {
		$('#modeid').html(result);
	});
};

// Set current volume in input text field
function handleVolumeReply() {
	socket.on('volumeReply', function(result) {
		$('#volumeID').val(result);
	});
};

// Set current tempo in input text field
function handleTempoReply() {
	socket.on('tempoReply', function(result) {
		$('#tempoID').val(result);
	});
};

// Display device's uptime in hours, minutes and seconds
function handleUptimeReply() {
	socket.on('uptimeReply', function(result) {
		$('#status').html(result);
	});
};

// Display server error
function displayServerError() {
	$('#error-box').show();
	$('#error-text').html('SERVER ERROR: No response from server. Is it running?');
	var serverErrorTimer = window.setTimeout(function() {
		$('#error-box').hide();		
	}, 10000);
	window.setTimeout(function() {
		clearTimeout(serverErrorTimer);
	}, 10000);

	
}


