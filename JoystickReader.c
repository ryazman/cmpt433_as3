/*
 *  JoystickReader reads the joystick input value on the BBB Zen Cape.
 *  It reads the GPIO inputs for the joystick directions (up, down, left, right)
 *  and 'in' press and modifies playback accordingly in BeatBox.
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "JoystickReader.h"
#include "BeatBox.h"

#define GPIO_UP 26
#define GPIO_DOWN 46
#define GPIO_LEFT 65
#define GPIO_RIGHT 47
#define GPIO_PUSHED 27

void exportGPIOFile(int gpioNum)
{
	FILE* exportFile = fopen("/sys/class/gpio/export", "w");
	if (exportFile == NULL) {
		printf("ERROR: Unable to open export file.\n");
		exit(1);
	}
	fprintf(exportFile, "%d", gpioNum);
	fclose(exportFile);
}

void exportGPIOFiles()
{
	exportGPIOFile(GPIO_UP);
	exportGPIOFile(GPIO_DOWN);
	exportGPIOFile(GPIO_LEFT);
	exportGPIOFile(GPIO_RIGHT);
	exportGPIOFile(GPIO_PUSHED);
}

int readJoystickUpVal()
{
	char* jsUpFileName = "/sys/class/gpio/gpio26/value";
	FILE* jsUpFile = fopen(jsUpFileName, "r");
	if (jsUpFile == NULL) {
		printf("ERROR: Unable to open file (%s) for read\n", jsUpFileName);
	}
	const int max_length = 1024;
	char buff[max_length];
	fgets(buff, max_length, jsUpFile);
	fclose(jsUpFile);
	return atoi(buff);
}

int readJoystickDownVal()
{
	char* jsDownFileName = "/sys/class/gpio/gpio46/value";
	FILE* jsDownFile = fopen(jsDownFileName, "r");
	if (jsDownFile == NULL) {
		printf("ERROR: Unable to open file (%s) for read\n", jsDownFileName);
	}
	const int max_length = 1024;
	char buff[max_length];
	fgets(buff, max_length, jsDownFile);
	fclose(jsDownFile);
	return atoi(buff);
}

int readJoystickLeftVal()
{
	char* jsLeftFileName = "/sys/class/gpio/gpio65/value";
	FILE* jsLeftFile = fopen(jsLeftFileName, "r");
	if (jsLeftFile == NULL) {
		printf("ERROR: Unable to open file (%s) for read\n", jsLeftFileName);
	}
	const int max_length = 1024;
	char buff[max_length];
	fgets(buff, max_length, jsLeftFile);
	fclose(jsLeftFile);
	return atoi(buff);
}

int readJoystickRightVal()
{
	char* jsRightFileName = "/sys/class/gpio/gpio47/value";
	FILE* jsRightFile = fopen(jsRightFileName, "r");
	if (jsRightFile == NULL) {
		printf("ERROR: Unable to open file (%s) for read\n", jsRightFileName);
	}
	const int max_length = 1024;
	char buff[max_length];
	fgets(buff, max_length, jsRightFile);
	fclose(jsRightFile);
	return atoi(buff);
}

int readJoystickPushedVal()
{
	char* jsPushedFileName = "/sys/class/gpio/gpio27/value";
	FILE* jsPushedFile = fopen(jsPushedFileName, "r");
	if (jsPushedFile == NULL) {
		printf("ERROR: Unable to open file (%s) for read\n", jsPushedFileName);
	}
	const int max_length = 1024;
	char buff[max_length];
	fgets(buff, max_length, jsPushedFile);
	fclose(jsPushedFile);
	return atoi(buff);
}

// nanosleep code provided by
// timmurphy.com/2009/09/29/nanosleep-in-c-c
void sleep(int msecs) {
	struct timespec req = {0};
	req.tv_sec = 0;
	req.tv_nsec = msecs * 1000000L;
	nanosleep(&req, (struct timespec *)NULL);
}

void* JoystickReader_readValues()
{
	exportGPIOFiles();

	while (!BeatBox_isFinished()) {
		if (readJoystickPushedVal() == 0) {
			BeatBox_nextBeat();
		} else if (readJoystickUpVal() == 0) {
			BeatBox_increaseVolume();
		} else if (readJoystickDownVal() == 0) {
			BeatBox_decreaseVolume();
		} else if (readJoystickRightVal() == 0) {
			BeatBox_increaseTempo();
		} else if (readJoystickLeftVal() == 0) {
			BeatBox_decreaseTempo();
		}
		sleep(250);
	}
	return NULL;
}


int JoystickReader_createJoystickReaderThread()
{
	pthread_t joystickReaderID;
	pthread_create(&joystickReaderID, NULL, &JoystickReader_readValues, NULL);

	return joystickReaderID;
}
