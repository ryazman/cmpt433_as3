/*
 * BeatBox.c stores and returns information about the current beat, volume, tempo,
 * allows other modules to call functions to play sounds/beats and change volume/tempo,
 * and uses Audiomixer to process and store individual sound wav files.
 */
#include <time.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "audioMixer.h"
#include "BeatBox.h"
#include "JoystickReader.h"
#include "UDPListener.h"
#include "accelerometer.h"

#define BASE_DRUM "beatbox-wav-files/100051__menegass__gui-drum-bd-hard.wav"
#define HI_HAT "beatbox-wav-files/100053__menegass__gui-drum-cc.wav"
#define SNARE "beatbox-wav-files/100059__menegass__gui-drum-snare-soft.wav"

#define DEFAULT_BPM 120
#define MIN_BPM 40
#define MAX_BPM 300
#define MIN_VOL 0
#define MAX_VOL 100
#define VOL_STEP 5
#define BPM_STEP 5

int bpm = DEFAULT_BPM;

int beatSelector = 0;

_Bool finished;

wavedata_t baseDrum;
wavedata_t hiHat;
wavedata_t snare;

void* beatThread();

int main()
{
	finished = false;

	AudioMixer_init();
	AudioMixer_readWaveFileIntoMemory(BASE_DRUM, &baseDrum);
	AudioMixer_readWaveFileIntoMemory(HI_HAT, &hiHat);
	AudioMixer_readWaveFileIntoMemory(SNARE, &snare);
	pthread_t joystickReaderID = JoystickReader_createJoystickReaderThread();
	pthread_t udpListenerID = UDPListener_createUDPListenerThread();
	pthread_t beatID;
	pthread_create(&beatID, NULL, &beatThread, NULL);

	int accelID = Accelerometer_createAccelThread();
	pthread_join(beatID, NULL);
	pthread_join(accelID, NULL);
	pthread_join(joystickReaderID, NULL);
	pthread_join(udpListenerID, NULL);
	AudioMixer_freeWaveFileData(&baseDrum);
	AudioMixer_freeWaveFileData(&snare);
	AudioMixer_freeWaveFileData(&hiHat);
	AudioMixer_cleanup();
	return 0;
}

void* beatThread()
{
	double halfBeat =((60 / (double)bpm / 2) * 1000000000);
	struct timespec halfBeatSleep = {0};
		halfBeatSleep.tv_sec = 0;
		halfBeatSleep.tv_nsec = halfBeat;
	int halfBeatCount = 0;

	while (!BeatBox_isFinished()) {
		while (beatSelector == 0) {
			halfBeat = ((60 / (double)bpm / 2) * 1000000000);
			halfBeatSleep.tv_nsec = halfBeat;
			printf("halfbeat: %f\n", halfBeat);
			nanosleep(&halfBeatSleep, (struct timespec *)NULL);
		}
		while (beatSelector == 1) {
			halfBeat = ((60 / (double)bpm / 2) * 1000000000);
			halfBeatSleep.tv_nsec = halfBeat;
			printf("halfbeat: %f\n", halfBeat);
			if (halfBeatCount % 1 == 0) {
				AudioMixer_queueSound(&hiHat);
			}
			if (halfBeatCount % 4 == 0) {
				AudioMixer_queueSound(&baseDrum);
			}
			if ((halfBeatCount +2) % 4 == 0) {
				AudioMixer_queueSound(&snare);
			}
			nanosleep(&halfBeatSleep, (struct timespec *)NULL);
			halfBeatCount++;
			}
		while (beatSelector == 2) {
			halfBeat = ((60 / (double)bpm / 2) * 1000000000);
			halfBeatSleep.tv_nsec = halfBeat;
			printf("halfbeat: %f\n", halfBeat);
			if (halfBeatCount % 2 == 0) {
				AudioMixer_queueSound(&hiHat);
			}
			if (halfBeatCount % 4 == 0) {
				AudioMixer_queueSound(&baseDrum);
			}
			if (halfBeatCount % 3 == 0) {
				AudioMixer_queueSound(&snare);
			}
			nanosleep(&halfBeatSleep, (struct timespec *)NULL);
			halfBeatCount++;
		}
	}
	return NULL;
}

void BeatBox_queueHiHat() {
	AudioMixer_queueSound(&hiHat);
}

void BeatBox_queueSnare() {
	AudioMixer_queueSound(&snare);
}

void BeatBox_queueBaseDrum() {
	AudioMixer_queueSound(&baseDrum);
}

void BeatBox_nextBeat() {
	beatSelector = (beatSelector+1) % 3;
	printf("Changing to beat: %s!\n", BeatBox_getCurrBeatName());
}

char* BeatBox_getCurrBeatName() {
	char* currBeatName;
	if (beatSelector == 0) {
		currBeatName = "None";
	} else if (beatSelector == 1) {
		currBeatName = "Rock 1";
	} else if (beatSelector == 2) {
		currBeatName = "Rock 2";
	}
	return currBeatName;
}

void BeatBox_changeBeat(char* beatNum) {
	beatSelector = atoi(beatNum);
}

void BeatBox_increaseVolume() {
	int volume = AudioMixer_getVolume();
	if (volume <= (MAX_VOL - VOL_STEP)) {
		volume += VOL_STEP;
	} else if (volume > (MAX_VOL - VOL_STEP) && volume < MAX_VOL) {
		volume = MAX_VOL;
	}
	AudioMixer_setVolume(volume);
}

void BeatBox_decreaseVolume() {
	int volume = AudioMixer_getVolume();
	if (volume >= (MIN_VOL + VOL_STEP)) {
		volume -= VOL_STEP;
	} else if (volume < (MIN_VOL + VOL_STEP) && volume > MIN_VOL) {
		volume = MIN_VOL;
	}
	AudioMixer_setVolume(volume);
}

void BeatBox_increaseTempo() {
	if (bpm <= (MAX_BPM - BPM_STEP)) {
		bpm += BPM_STEP;
	} else if (bpm > (MAX_BPM - BPM_STEP) && bpm < MAX_BPM) {
		bpm = MAX_BPM;
	}
}

void BeatBox_decreaseTempo() {
	if (bpm >= (MIN_BPM + BPM_STEP)) {
		bpm -= BPM_STEP;
	} else if (bpm < (MIN_BPM + BPM_STEP) && bpm > MIN_BPM) {
		bpm = MIN_BPM;
	}
}

void BeatBox_playSound(char* soundName) {
	if (strcmp(soundName, "h") == 0) {
		BeatBox_queueHiHat();
	} else if (strcmp(soundName, "b") == 0) {
		BeatBox_queueBaseDrum();
	} else if (strcmp(soundName, "s") == 0) {
		BeatBox_queueSnare();
	} else {
	}
}

int BeatBox_getTempo() {
	return bpm;
}

int BeatBox_getVolume() {
	return AudioMixer_getVolume();
}

_Bool BeatBox_isFinished() {
	return finished;
}

