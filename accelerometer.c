/*
 * Accelerometer.c  starts a thread which writes to the I2C to make reading from it possible, then reads the values of the
 * accelerometer over and over again. If the values of X, Y or Z pass a certain threshold, queue
 * a sound using beatBox.c (which then calls audioMixer.c) to play. Able to shake the BBB to create a
 * BPM of around 120, but you cannot play two sounds at once.
 */

#include <time.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <inttypes.h>
#include "BeatBox.h"

#define I2CDRV_LINUX_BUS0 "/dev/i2c-0"
#define I2CDRV_LINUX_BUS1 "/dev/i2c-1"
#define I2CDRV_LINUX_BUS2 "/dev/i2c-2"

#define I2C_DEVICE_ADDRESS 0x1C
#define REG_START 0x00
#define REG_ACTIVE 0x2A

#define X_MSB 1
#define X_LSB 2
#define Y_MSB 3
#define Y_LSB 4
#define Z_MSB 5
#define Z_LSB 6

#define BYTES 7

static int initI2cBus(char* bus, int address);
static void readI2cReg(int i2cFileDesc, unsigned char regAddr, int bytes, unsigned char *values);
static void writeI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char value);
void* airDrummingThread();

int Accelerometer_createAccelThread()
{
	pthread_t accelID;
	pthread_create(&accelID, NULL, &airDrummingThread, NULL);
	return accelID;
}

void* airDrummingThread()
{
	struct timespec tenMSSleep = {0};
		tenMSSleep.tv_sec = 0;
		tenMSSleep.tv_nsec = 10000000;

	struct timespec hundredMSSleep = {0};
		hundredMSSleep.tv_sec = 0;
		hundredMSSleep.tv_nsec = 160000000;

	int i2cFileDesc = initI2cBus(I2CDRV_LINUX_BUS2, I2C_DEVICE_ADDRESS);
	writeI2cReg(i2cFileDesc, REG_ACTIVE, 0x01);

	unsigned char *values = malloc(sizeof(*values) * BYTES);
	readI2cReg(i2cFileDesc, REG_START, BYTES, values);

	int16_t standardXVal =  ((values[X_MSB] << 8) | (values[X_LSB]));
	float standardGravityX = (((float)standardXVal) / 32768) * 2.981;

	int16_t standardYVal =  ((values[Y_MSB] << 8) | (values[Y_LSB]));
	float standardGravityY = (((float)standardYVal) / 32768) * 2.981;

	int16_t standardZVal =  ((values[Z_MSB] << 8) | (values[Z_LSB]));
	float standardGravityZ = (((float)standardZVal) / 32768) * 2.981;

	while (!BeatBox_isFinished()) {
		readI2cReg(i2cFileDesc, REG_START, BYTES, values);

		int16_t xVal =  ((values[X_MSB] << 8) | (values[X_LSB]));
		float gravityX = (((float)xVal) / 32768) * 2.981;

		int16_t yVal =  ((values[Y_MSB] << 8) | (values[Y_LSB]));
		float gravityY = (((float)yVal) / 32768) * 2.981;

		int16_t zVal =  ((values[Z_MSB] << 8) | (values[Z_LSB]));
		float gravityZ = (((float)zVal) / 32768) * 2.981;

		if ((gravityX - standardGravityX) > 0.7 || (gravityX - standardGravityX) < -0.7) {
			BeatBox_queueHiHat();
			nanosleep(&hundredMSSleep, (struct timespec *)NULL);

		}
		if ((gravityY - standardGravityY) > 0.7 || (gravityY - standardGravityY) < -0.7) {
			BeatBox_queueSnare();
			nanosleep(&hundredMSSleep, (struct timespec *)NULL);

		}
		if ((gravityZ - standardGravityZ) > 0.7 || (gravityZ - standardGravityZ) < -0.7) {
			BeatBox_queueBaseDrum();
			nanosleep(&hundredMSSleep, (struct timespec *)NULL);

		}

		nanosleep(&tenMSSleep, (struct timespec *)NULL);
	}
	return NULL;
}

static int initI2cBus(char* bus, int address)
{
	int i2cFileDesc = open(bus, O_RDWR);
	if (i2cFileDesc < 0) {
		printf("I2C DRV: Unable to open bus for read/write (%s)\n", bus);
		perror("Error is:");
		exit(-1);
	}

	int result = ioctl(i2cFileDesc, I2C_SLAVE, address);
	if (result < 0) {
		perror("Unable to set I2C device to slave address.");
		exit(-1);
	}
	return i2cFileDesc;
}

static void readI2cReg(int i2cFileDesc, unsigned char regAddr, int bytes, unsigned char *values)
{
	// To read a register, must first write the address
	int res = write(i2cFileDesc, &regAddr, sizeof(regAddr));
	if (res != sizeof(regAddr)) {
		perror("Unable to write i2c register.");
		exit(-1);
	}

	// Now read the value and return it
	res = read(i2cFileDesc, values, bytes);
	if (res != bytes) {
		perror("Unable to read i2c register");
		exit(-1);
	}
}

static void writeI2cReg(int i2cFileDesc, unsigned char regAddr, unsigned char value)
{
	unsigned char buff[2];
	buff[0] = regAddr;
	buff[1] = value;
	int res = write(i2cFileDesc, buff, 2);
	if (res != 2) {
		perror("Unable to write i2c register");
		exit(-1);
	}
}

