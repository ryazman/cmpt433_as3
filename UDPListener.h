/*
 * UDPListener header file
 */

#ifndef _UDP_LISTENER_H_
#define _UDP_LISTENER_H_

int UDPListener_createUDPListenerThread();
_Bool UDPListener_isFinished();

#endif
